/*
 * @Description:
 * @Author: 猪脚饭
 * @Date: 2022-11-15 11:15:05
 * @LastEditors: 猪脚饭
 * @LastEditTime: 2022-11-15 11:33:43
 */

import store from '@/store'
// 导出一个函数判断该用户没有该按钮的权限
export default {
  methods: {
    checkBtn(code) {
      // 先把该登录用户的按钮权限信息拿到
      const checkBtnList = store.state.user.userInfo
      // 判断一下，并且返回结果
      return checkBtnList && checkBtnList.points.includes(code)
    }
  }
}

