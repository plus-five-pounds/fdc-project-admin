import PageTools from './PageTools'
import ImageUpload from './ImageUpload'
import ScreenFull from './ScreenFull'
import Pagination from './Pagination'
export default {
  install(vue) {
    vue.component('PageTools', PageTools)
    vue.component('ImageUpload', ImageUpload)
    vue.component('ScreenFull', ScreenFull)
    vue.component('Pagination', Pagination)
  }
}
