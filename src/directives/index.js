import img from '@/assets/common/show.png'
const directives = {
  imgError: {
    inserted(dom, options) {
      // 图片加载失败触发
      dom.addEventListener('error', () => {
        dom.src = img
      })
      dom.src = dom.src || img
    },
    componentUpdated(dom, options) {
      dom.src = dom.src || img
    }
  }
}

export default {
  install(vue) {
    for (const key in directives) {
      vue.directive(key, directives[key])
    }
  }
}
