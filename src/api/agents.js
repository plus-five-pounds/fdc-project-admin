import request from '@/utils/request'
// 获取经纪人列表信息
export function getAgentsList(params) {
  return request({
    url: '/agents',
    params
  })
}
// 添加经纪
export function addAgent(data) {
  return request({
    url: '/agents',
    method: 'POST',
    data
  })
}
// 获取经纪详情
export function getAgentData(id) {
  return request({
    url: `/agents/${id}`
  })
}
// 编辑经纪
export function editAgent(data) {
  return request({
    url: `/agents/${data.id}`,
    method: 'PUT',
    data
  })
}
// 删除经纪
export function delAgent(id) {
  return request({
    url: `/agents/${id}`,
    method: 'DELETE'
  })
}
// 获取经纪数量
export function getAgentCount(params) {
  return request({
    url: '/agents/count',
    params
  })
}
