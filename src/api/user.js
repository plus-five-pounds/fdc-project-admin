import request from '@/utils/request'
// 登录
export function login(data) {
  return request({
    url: '/auth/local',
    method: 'post',
    data
  })
}
// 获取用户资料
export function getUserInfo() {
  return request({
    url: '/users/me'
  })
}

// 获取用户权限
export function getPermissions() {
  return request({
    url: '/mypermissions'
  })
}
export function logout() {
  return request({
    url: '/vue-admin-template/user/logout',
    method: 'post'
  })
}
// 注册
export function register(data) {
  return request({
    url: '/auth/local/register',
    method: 'POST',
    data
  })
}
