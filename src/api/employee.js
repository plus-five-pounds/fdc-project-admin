/*
 * @Description: Description
 * @Author: Xeni
 * @Date: 2022-11-13 10:02:59
 * @LastEditors: Xeni
 * @LastEditTime: 2022-11-13 18:26:47
 */
// 封装员工相关接口
// 1、导入axios
import request from '@/utils/request'
// 获取用户列表接口函数
export function employeeList(params) {
  // params期待传入{_limit：每页条数，_start：开始获取的位置}
  return request({
    url: '/users',
    method: 'get',
    params
  })
}
// 获取用户数量
export function employeeCount() {
  return request({
    url: '/users/count',
    method: 'get'
  })
}
// 根据id删除员工
export function deleteEmployee(id) {
  return request({
    url: '/users/' + id,
    method: 'delete'
  })
}
// 根据id获取指定用户信息
export function employeeById(id) {
  return request({
    url: '/users/' + id,
    method: 'get'
  })
}
// 添加员工
export function addEmployee(data) {
  return request({
    url: '/auth/local/register',
    method: 'post',
    data
  })
}
// 根据id编辑员工接口
export function updateEmployee(data) {
  return request({
    url: '/users/' + data.id,
    method: 'put',
    data
  })
}
