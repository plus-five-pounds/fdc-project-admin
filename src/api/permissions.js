import request from '@/utils/request'
// 获取权限列表
export function getPermList(params) {
  return request({
    url: '/permissions',
    method: 'get',
    params
  })
}
// 获取权限数量
export function getPermTotal() {
  return request({
    url: '/permissions/count',
    method: 'get'

  })
}

// 新建权限
export function addPerm(data) {
  return request({
    url: '/permissions',
    method: 'post',
    data

  })
}
// 获取指定权限
export function getAssignPerm(id) {
  return request({
    url: '/permissions/' + id,
    method: 'get'

  })
}

// 删除权限
export function delPerm(id) {
  return request({
    url: '/permissions/' + id,
    method: 'DELETE'

  })
}

// 编辑权限
export function editPerm(data) {
  return request({
    url: '/permissions/' + data.id,
    method: 'put',
    data

  })
}
