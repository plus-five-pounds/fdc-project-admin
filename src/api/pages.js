/*
 * @Description: Description
 * @Author: Xeni
 * @Date: 2022-11-14 10:08:21
 * @LastEditors: Xeni
 * @LastEditTime: 2022-11-14 17:38:51
 */
// 封装页面管理相关接口函数
// 1、导入axios
import request from '@/utils/request'
// 获取页面列表接口函数
export function pageList(params) {
  // params期待传入{_limit每页的获取条数，_start开始获取的位置，title_contains模糊搜索标题},不传参数默认获取所有页面列表
  return request({
    url: '/pages',
    method: 'get',
    params
  })
}
// 获取页面数量
export function getPagesCount(params) {
  // params可以传入{title_contains，模糊搜索标题}，也可以不传
  return request({
    url: '/pages/count',
    method: 'get',
    params
  })
}
// 添加页面接口函数
export function addPages(data) {
  // data期待传入{title,content}
  return request({
    url: '/pages',
    method: 'post',
    data
  })
}
// 根据id删除页面
export function deletePage(id) {
  return request({
    url: '/pages/' + id,
    method: 'delete'
  })
}
// 根据id获取指定页面信息
export function getPageById(id) {
  return request({
    url: '/pages/' + id,
    method: 'get'
  })
}
// 根据id编辑指定页面
export function updatePage(data) {
  return request({
    url: '/pages/' + data.id,
    method: 'put',
    data
  })
}
