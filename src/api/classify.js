/*
 * @Description:
 * @Author: 猪脚饭
 * @Date: 2022-11-14 09:26:45
 * @LastEditors: 猪脚饭
 * @LastEditTime: 2022-11-14 15:59:30
 */
import request from '@/utils/request'

// 获取分类列表
export function getCategories(params) {
  return request({
    url: '/categories',
    method: 'get',
    params
  })
}
// 获取分类数量
export function getClassCount(params) {
  return request({
    url: '/categories/count',
    method: 'get',
    params
  })
}
// 新建分类
export function addClass(data) {
  return request({
    url: '/categories',
    method: 'post',
    data
  })
}
// 获取指定分类
export function getDetailCategories(id) {
  return request({
    url: `/categories/${id}`,
    method: 'get'
  })
}
// 编辑分类
export function updateCategories(data) {
  return request({
    url: `/categories/${data.id}`,
    method: 'put',
    data
  })
}
// 删除按钮
export function deleteCategories(id) {
  return request({
    url: `/categories/${id}`,
    method: 'delete'
  })
}
