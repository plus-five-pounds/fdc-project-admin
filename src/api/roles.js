/*
 * @Description:
 * @Author: 猪脚饭
 * @Date: 2022-11-13 09:38:55
 * @LastEditors: 猪脚饭
 * @LastEditTime: 2022-11-13 20:37:57
 */
import request from '@/utils/request'
// 获取角色列表
export function getRoles(params) {
  return request({
    url: '/roles',
    method: 'get',
    params
  })
}
// 获取对应的角色详细信息
export function getRolesDetail(id) {
  return request({
    url: `/roles/${id}`,
    method: 'get'
  })
}
// 获取角色总数量
export function getRoleCount() {
  return request({
    url: '/roles/count'
  })
}
// 新增角色
export function addRoles(data) {
  return request({
    url: '/roles',
    method: 'post',
    data
  })
}
// 编辑角色
export function editRoles(data) {
  return request({
    url: `/roles/${data.id}`,
    method: 'put',
    data
  })
}
// 删除角色
export function deletePermission(id) {
  return request({
    url: `/roles/${id}`,
    method: 'delete'
  })
}
//  获取权限列表
export function getPermission(params) {
  return request({
    url: '/permissions',
    method: 'get',
    params
  })
}
