// 导入封装好的接口函数
import request from '@/utils/request.js'
// 获取房产列表
export function getPropertiesList(params) {
  return request({
    url: '/properties',
    method: 'get',
    params
  })
}
// 获取房产列表总条数
export function getPropertiesCount(params) {
  return request({
    url: '/properties/count',
    method: 'get',
    params
  })
}
// 添加房产列表
export function addPropertiesList(data) {
  return request({
    url: '/properties',
    method: 'post',
    data
  })
}
// 删除房产列表
export function delPropertiesList(id) {
  return request({
    url: `/properties/${id}`,
    method: 'delete'
  })
}
// 获取房产详情列表
export function getPropertiesDetail(id) {
  return request({
    url: `/properties/${id}`,
    method: 'get'
  })
}
// 修改房产列表
export function updatePropertiesList(data) {
  return request({
    url: `/properties/${data.id}`,
    method: 'put',
    data
  })
}
