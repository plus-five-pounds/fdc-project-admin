// 封装客户管理请求接口
import request from '@/utils/request'

// 封装获取客户列表数据接口
export function getClientelePort(params) {
  return request({
    url: '/clients',
    method: 'get',
    params
  })
}

// 封装删除客户接口
export function delClientelePort(id) {
  request({
    url: `/clients/${id}`,
    method: 'DELETE'
  })
}

// 封装添加数据
export function addClientelePort(data) {
  return request({
    url: '/clients',
    method: 'post',
    data
  })
}

// 封装编辑接口
export function putClientelePort(data) {
  request({
    url: `/clients/${data.id}`,
    method: 'put',
    data
  })
}

// 封装根据id获取客户信息接口
export function geIdClientsPort(params) {
  return request({
    url: `/clients`,
    method: 'get',
    params
  })
}

// 封装搜索名称获取数据接口
export function getNameClientPort(params) {
  return request({
    url: '/clients',
    method: 'get',
    params
  })
}

// 封装获取客户总数量
export function getClientNumberPort() {
  return request({
    url: '/clients/count',
    method: 'get'
  })
}
