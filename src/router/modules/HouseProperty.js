import Layout from '@/layout'
export default {
  path: '/HouseProperty',
  component: Layout,
  code: 'getPropertiesList',
  children: [{
    path: '',
    name: 'HouseProperty',
    component: () => import('@/views/HouseProperty/index.vue'),
    meta: { title: '房产管理', icon: 'size' }
  }]
}
