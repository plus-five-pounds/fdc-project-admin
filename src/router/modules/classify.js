/*
 * @Description:
 * @Author: 猪脚饭
 * @Date: 2022-11-14 09:00:16
 * @LastEditors: 猪脚饭
 * @LastEditTime: 2022-11-14 09:03:03
 */
import Layout from '@/layout'
export default {
  path: '/classify',
  component: Layout,
  code: 'getCategoriesList',
  children: [{
    path: '',
    name: 'classify',
    component: () => import('@/views/classify/index'),
    meta: { title: '分类管理', icon: 'table' }
  }]
}
