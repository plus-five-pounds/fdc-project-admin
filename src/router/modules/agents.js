import Layout from '@/layout'
export default {
  path: '/agents',
  component: Layout,
  code: 'getAgentsList',
  children: [{
    path: '',
    name: 'agents',
    component: () => import('@/views/agents/index'),
    meta: { title: '经纪管理', icon: 'star' }
  }]
}
