import Layout from '@/layout'
export default {
  path: '/clientele',
  component: Layout,
  code: 'getAgentsList',
  children: [
    {
      path: 'index',
      name: 'Clientele',
      component: () => import('@/views/clientele/index'),
      meta: { title: '客户管理', icon: 'people' }
    }
  ]
}
