/*
 * @Description:
 * @Author: 猪脚饭
 * @Date: 2022-11-13 09:16:21
 * @LastEditors: 猪脚饭
 * @LastEditTime: 2022-11-13 09:25:52
 */
// 首先导入layout
import Layout from '@/layout'
export default {
  path: '/roles',
  component: Layout,
  code: 'getRolesList',
  children: [{
    path: '',
    name: 'roles',
    component: () => import('@/views/roles/index'),
    meta: { title: '角色管理', icon: 'tree' }
  }]
}
