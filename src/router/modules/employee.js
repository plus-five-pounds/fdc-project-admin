/*
 * @Description: Description
 * @Author: Xeni
 * @Date: 2022-11-13 09:20:41
 * @LastEditors: Xeni
 * @LastEditTime: 2022-11-13 09:27:19
 */
import Layout from '@/layout'
export default {
  path: '/employee',
  component: Layout,
  code: 'getUsersList',
  children: [
    {
      path: '',
      name: 'employee',
      component: () => import('@/views/employee/index.vue'),
      meta: { title: '员工管理', icon: 'user' }
    }
  ]
}
