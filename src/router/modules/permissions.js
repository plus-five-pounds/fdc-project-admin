// 引入一级路由页面
import Layout from '@/layout'

// 导出路径
export default {
  path: '/permissions',
  component: Layout, // 注册父组件
  code: 'getPermissionsList',
  children: [{
    path: '',
    name: 'permissions',
    component: () => import('@/views/permissions/index'),
    meta: { title: '权限管理', icon: 'list' }
  }]
}
