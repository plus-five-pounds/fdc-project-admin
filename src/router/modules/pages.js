/*
 * @Description: Description
 * @Author: Xeni
 * @Date: 2022-11-14 09:37:45
 * @LastEditors: Xeni
 * @LastEditTime: 2022-11-14 09:48:41
 */
// 导入layout组件
import Layout from '@/layout'
// 导出动态路由
export default {
  path: '/pages',
  component: Layout,
  code: 'getUsersList',
  children: [
    {
      path: '',
      name: 'pages',
      component: () => import('@/views/pages/index.vue'),
      meta: { title: '页面管理', icon: 'tab' }
    }
  ]
}
