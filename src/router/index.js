/*
 * @Description:
 * @Author: 猪脚饭
 * @Date: 2022-11-15 09:17:55
 * @LastEditors: 猪脚饭
 * @LastEditTime: 2022-11-15 14:54:26
 */
import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

/* Layout */
import Layout from '@/layout'
import agents from './modules/agents'
import employee from './modules/employee'
import permissions from './modules/permissions'
import roles from './modules/roles'
import HouseProperty from './modules/HouseProperty'
import classify from './modules/classify'
import pages from './modules/pages'
import clientele from './modules/clientele'

// 导出动态路由
export const asyncRoutes = [
  agents,
  employee,
  permissions,
  roles,
  HouseProperty,
  classify,
  pages,
  clientele
]

export const constantRoutes = [
  {
    path: '/login',
    component: () => import('@/views/login/index'),
    hidden: true
  },
  {
    path: '/register',
    component: () => import('@/views/register/index'),
    hidden: true
  },

  {
    path: '/404',
    component: () => import('@/views/404'),
    hidden: true
  },

  {
    path: '/',
    component: Layout,
    redirect: '/dashboard',
    children: [{
      path: 'dashboard',
      name: 'Dashboard',
      component: () => import('@/views/dashboard/index'),
      meta: { title: '首页', icon: 'dashboard' }
    }]
  }
  // 404 page must be placed at the end !!!
  // { path: '*', redirect: '/404', hidden: true }
]

const createRouter = () => new Router({
  // mode: 'history', // require service support
  scrollBehavior: () => ({ y: 0 }),
  routes: [...constantRoutes]
})

const router = createRouter()

// Detail see: https://github.com/vuejs/vue-router/issues/1234#issuecomment-357941465
export function resetRouter() {
  const newRouter = createRouter()
  router.matcher = newRouter.matcher // reset router
}

export default router
