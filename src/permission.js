/*
 * @Description:
 * @Author: 猪脚饭
 * @Date: 2022-11-14 11:14:13
 * @LastEditors: 猪脚饭
 * @LastEditTime: 2022-11-15 16:12:53
 */
import router from './router'
import NProgress from 'nprogress' // progress bar
import 'nprogress/nprogress.css' // progress bar style
import store from '@/store'

const whiteList = ['/login', '/register', '/404'] // no redirect whitelist
router.beforeEach(async(to, from, next) => {
  const token = store.getters.token
  if (token) {
    if (to.path === '/login') next('/')
    else {
      if (!store.state.user.userInfo.id) {
        // 调用接口获取用户信息并且保存再UserInfo里面
        await store.dispatch('user/getUserInfo')
        // 筛选动态路由
        const filterRouter = await store.dispatch('permissions/filterRouter', store.state.user.userInfo.menus)
        // 把筛选出来的动态路由添加到路由规则当中
        router.addRoutes([...filterRouter, { path: '*', redirect: '/404', hidden: true }])
        // 这里再写是因为to.path已经匹配了，不会重新匹配，需要重新来一次
        return next(to.path)
      }
      next()
    }
  } else {
    if (whiteList.indexOf(to.path) !== -1) next()
    else {
      next('/login')
    }
  }
})

router.afterEach(() => {
  // finish progress bar
  NProgress.done()
})
