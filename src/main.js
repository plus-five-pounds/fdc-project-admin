/*
 * @Description:
 * @Author: 猪脚饭
 * @Date: 2022-11-15 08:55:02
 * @LastEditors: 猪脚饭
 * @LastEditTime: 2022-11-15 11:32:58
 */
import Vue from 'vue'

import 'normalize.css/normalize.css' // A modern alternative to CSS resets

import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import locale from 'element-ui/lib/locale/lang/en' // lang i18n

import '@/styles/index.scss' // global css

import App from './App'
import store from './store'
import router from './router'

import '@/icons' // icon
import '@/permission' // permission control
import components from '@/components'
import directives from '@/directives'
Vue.use(components)
Vue.use(directives)
/**
 * If you don't want to use mock-server
 * you want to use MockJs for mock api
 * you can execute: mockXHR()
 *
 * Currently MockJs will be used in the production environment,
 * please remove it before going online ! ! !
 */
// set ElementUI lang to EN
Vue.use(ElementUI, { locale })
// 如果想要中文版 element-ui，按如下方式声明
// Vue.use(ElementUI)

Vue.config.productionTip = false

// 全局自定义指令-图片
Vue.directive('imgErr', {
  // inserted函数只有在标签第一次插入到html结构中的时候才会触发 并且只会触发一次
  inserted(el, binding) {
    // 监听错误事件
    el.addEventListener('error', () => {
      // console.log('----->30', binding.value)
      // 获取img中的src，将图片赋值给它
      el.src = binding.value
    })
    // 判断后端是否有返回图片，有则不执行，没有则赋值自定义图片
    el.src = el.src || binding.value
  }
})
// 全局注册混入对象mixin
import MyMixin from '@/mixin/index'
Vue.mixin(MyMixin)
new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App)
})
