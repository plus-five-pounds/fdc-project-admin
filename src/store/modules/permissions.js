/*
 * @Description:
 * @Author: 猪脚饭
 * @Date: 2022-11-15 09:07:13
 * @LastEditors: 猪脚饭
 * @LastEditTime: 2022-11-15 10:38:10
 */
// 导入动态路由
import { asyncRoutes, constantRoutes } from '@/router/index'
const state = {
  // 存放筛选后的总数路由
  routes: []
}
const mutations = {
  // 保存筛选后的路由
  setRoutes(state, routes) {
    state.routes = [...constantRoutes, ...routes]
  }
}
const actions = {
  // 筛选路由的函数
  filterRouter(store, menus) {
    const routes = asyncRoutes.filter(item => {
      return menus.includes(item.code)
    })
    // 保存筛选出来的路由
    store.commit('setRoutes', routes)
    // 返回路由
    return routes
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}

