import { login, getUserInfo, getPermissions } from '@/api/user'
import { setToken, getToken, removeToken } from '@/utils/auth'
import { resetRouter } from '@/router/index'
const state = {
  token: getToken(),
  userInfo: {}
}

const mutations = {
  setToken(state, token) {
    // 保存token
    state.token = token
    setToken(token)
  },
  // 保存用户资料
  setUserInfo(state, userInfo) {
    state.userInfo = userInfo
  },
  // 移除token
  removeToken(state) {
    state.token = ''
    removeToken()
  },
  // 移除用户资料
  removeUserInfo(state) {
    state.userInfo = {}
  }
}

const actions = {
  // 登录
  async login(ctx, data) {
    const { jwt } = await login(data)
    ctx.commit('setToken', jwt)
  },
  // 获取用户资料
  async getUserInfo(ctx) {
    const res = await getUserInfo()
    const data = await getPermissions()
    ctx.commit('setUserInfo', { ...res, ...data })
  },
  // 退出登录
  logout(ctx) {
    ctx.commit('removeToken')
    ctx.commit('removeUserInfo')
    resetRouter()
  }

}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}

