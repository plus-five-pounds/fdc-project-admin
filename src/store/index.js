/*
 * @Description:
 * @Author: 猪脚饭
 * @Date: 2022-11-13 09:06:54
 * @LastEditors: 猪脚饭
 * @LastEditTime: 2022-11-15 10:08:12
 */
import Vue from 'vue'
import Vuex from 'vuex'
import getters from './getters'
import app from './modules/app'
import settings from './modules/settings'
import user from './modules/user'
import permissions from '@/store/modules/permissions'

Vue.use(Vuex)

const store = new Vuex.Store({
  modules: {
    app,
    settings,
    user,
    permissions
  },
  getters
})

export default store
