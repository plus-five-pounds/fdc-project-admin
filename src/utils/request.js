import axios from 'axios'
import store from '@/store'
import { Message } from 'element-ui'
import router from '@/router'
// create an axios instance
const service = axios.create({
  baseURL: process.env.VUE_APP_BASE_API, // url = base url + request url
  // withCredentials: true, // send cookies when cross-domain requests
  timeout: 5000 // request timeout
})

// 添加请求拦截器
service.interceptors.request.use(function(config) {
  // 在发送请求之前做些什么
  const token = store.state.user.token
  // 注入全局token
  if (token && config.url !== '/auth/local/register') config.headers.Authorization = `Bearer ${token}`
  return config
}, function(error) {
  // 对请求错误做些什么
  return Promise.reject(error)
})

// 添加响应拦截器
service.interceptors.response.use(function(response) {
  // 2xx 范围内的状态码都会触发该函数。
  // 对响应数据做点什么
  const { data } = response
  return data
}, function(error) {
  if (error.response.data && error.response.data.statusCode === 401) {
    store.dispatch('user/logout')
    router.push('/login')
    Message.error('身份认证失败，请重新登录!')
  }
  Message.error('请求出现错误')
  // 超出 2xx 范围的状态码都会触发该函数。
  // 对响应错误做点什么
  return Promise.reject(error)
})

export default service
